#!/bin/sh

# Include the wrappers utility script
. /usr/lib/java-wrappers/java-wrappers.sh

# We need a java runtime (any 1.4 work)
find_java_runtime all

# Define our classpath
find_jars jardiff commons-cli asm asm-commons

# Run JarDiff
run_java org.osjava.jardiff.Main $extra_args "$@"

