jardiff (0.2-6) unstable; urgency=medium

  * Team upload.
  * Raising Standards version to 4.7.0:
    - Priority set to optional, from extra
    - Using https form of the Format in d/copyright
    - Setting Rules-Requires-Root: no
  * Specifying the classpath in build.xml, keeping d/rules as simple as
    possible
  * Installing an unversioned symlink to the jar
  * Updating the path to the built jar for dh_install
  * Raising d/watch version to 4
  * Removing unneeded versioned (Build-)Dependencies
  * Build-Depending on the headless JDK only
  * Adding missing patch headers

  [ Markus Koschany ]
  * Moved the package to Git
  * debhelper-compat = 13
  * Switch from cdbs to dh

 -- Pierre Gruet <pgt@debian.org>  Sun, 02 Feb 2025 14:19:53 +0100

jardiff (0.2-5.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Wed, 06 Jan 2021 16:26:21 +0100

jardiff (0.2-5) unstable; urgency=medium

  * Team upload.
  * Depend on libasm-java (>= 5.0) instead of libasm4-java
  * Standards-Version updated to 3.9.8

 -- Emmanuel Bourg <ebourg@apache.org>  Mon, 03 Oct 2016 14:58:43 +0200

jardiff (0.2-4) unstable; urgency=medium

  * Team upload.
  * Generate Java 5 compatible bytecode
  * debian/control:
    - Standards-Version updated to 3.9.5 (no changes)
    - Depend on libasm4-java instead of libasm3-java
    - Use canonical URLs for the Vcs-* fields
  * Switch to debhelper level 9
  * debian/copyright: Updated to the Copyright Format 1.0

 -- Emmanuel Bourg <ebourg@apache.org>  Mon, 15 Sep 2014 23:32:43 +0200

jardiff (0.2-3) unstable; urgency=low

  * Team upload.
  * asm2 -> asm3 migration
  * Convert to 3.0 source format.
  * Update standards version to 3.8.4.

 -- Onkar Shinde <onkarshinde@ubuntu.com>  Mon, 14 Jun 2010 18:24:51 +0530

jardiff (0.2-2) unstable; urgency=low

  * Use default-jdk:
    - Build-Depend on default-jdk instead of java-gcj-compat-dev
    - Depends on default-jre-headless
    - Use default-java from default-jdk as JAVA_HOME
  * Bump Standards-Version to 3.8.3:
    - Add ${misc:Depends} in Depends field for debhelper
    - Move to "java" section of archive
  * Set Maintainer field to Debian Java Team
  * Update Vcs-* fields
  * Add myself as Uploaders
  * Update my email address

 -- Damien Raude-Morvan <drazzib@debian.org>  Sat, 26 Sep 2009 20:19:07 +0200

jardiff (0.2-1) unstable; urgency=low

  * Initial release (Closes: #495998)

 -- Damien Raude-Morvan <drazzib@drazzib.com>  Mon, 13 Oct 2008 23:02:09 +0200
